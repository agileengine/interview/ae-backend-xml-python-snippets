from bs4 import BeautifulSoup

if __name__ == '__main__':
    sample_file_path = "../samples/startbootstrap-freelancer-gh-pages-cut.html"
    sample_soup = BeautifulSoup(open(sample_file_path, "r").read(), features="lxml")

    sendMessageButton = sample_soup.find_all(None, attrs={"id": "sendMessageButton"})
    print("Button by ID:", sendMessageButton)

    primaryButton = sample_soup.select("div[id=\"success\"] button[class*=\"btn-primary\"]")
    print("Button by CSS selector:", primaryButton)
